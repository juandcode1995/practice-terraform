resource "azurerm_resource_group" "rg"{
    name = "DevopsDiego"
    location = var.location
}

resource "azurerm_availability_set" "set"{
    name = "Set"
    location = var.location
    resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_virtual_network" "vnet"{
    name = "VNet"
    address_space = ["10.0.0.0/16"]
    location = var.location
    resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet"{
    name = "subnet1"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = ["10.0.0.0/17"]

}
resource "azurerm_subnet" "subnet2"{
    name = "subnet2"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = ["10.0.128.0/17"]

}

resource "azurerm_public_ip" "publicip" {
  name                = "PublicIP"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku = "Basic"

  tags = {
    environment = var.env
  }
}

resource "azurerm_public_ip" "publicip_dev" {
  name                = "PublicIPdev"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku = "Basic"

  tags = {
    environment = "dev"
  }
}

resource "azurerm_network_security_group" "general_security" {
  name = "${var.env}-${var.product}-windows-vm-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  security_rule {
    name                       = "allow-rdp"
    description                = "allow-rdp"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = var.sourceip
    destination_address_prefix = "*" 
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.windows_nic_subnet_1.id
  network_security_group_id = azurerm_network_security_group.general_security.id
}

resource "azurerm_network_interface" "windows_nic_subnet_1" {
  name                = "WindowsNIC"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "general"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.publicip.id
  }
}

resource "azurerm_network_interface" "windows_nic_subnet_2" {
  name                = "WindowsNIC_2"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "general"
    subnet_id                     = azurerm_subnet.subnet2.id
    private_ip_address_allocation = "Dynamic"
	public_ip_address_id = azurerm_public_ip.publicip_dev.id
  }
}

data "template_file" "powershell_init" {
  template = file("user-data.ps1")
}
data "template_file" "lcm_config" {
  template = file("LCMConfig.ps1")
}

resource "azurerm_windows_virtual_machine" "dev_enviroment" {
  name                = "ServerDev"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  size                = "Standard_DS1"
  admin_username      = "adminuser"
  admin_password      = var.password # move to vault or to get it from variables
  network_interface_ids = [
    azurerm_network_interface.windows_nic_subnet_1.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  #C:\AzureData
  custom_data = base64encode(data.template_file.powershell_init.rendered)

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}
resource "azurerm_windows_virtual_machine" "pro_enviroment" {
  name                = "ServerProd"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  size                = "Standard_DS1"
  admin_username      = "adminuser"
  admin_password      = var.password # move to vault or to get it from variables
  network_interface_ids = [
    azurerm_network_interface.windows_nic_subnet_2.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = var.windows-2016-sku
    version   = "latest"
  }
}


resource "azurerm_virtual_machine_extension" "execute-userdata" {
  #depends_on=[azurerm_windows_virtual_machine.web-windows-vm]
  name = "SetupGitlabRunnerAndUploadCM"
  virtual_machine_id = azurerm_windows_virtual_machine.dev_enviroment.id
  publisher = "Microsoft.Compute"
  type = "CustomScriptExtension"
  type_handler_version = "1.9"
  settings = <<SETTINGS
    { 
      "commandToExecute": "powershell New-Item -Path C:\\LCM\\LCMConfig.ps1 -Force && powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.lcm_config.rendered)}')) | Out-File -filepath C:\\LCM\\LCMConfig.ps1\";powershell rename-item  C:\\AzureData\\CustomData.bin -newname CustomData.ps1;powershell C:\\AzureData\\CustomData.ps1;"
    } 
  SETTINGS
}