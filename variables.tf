variable "location" {
  type    = string
}

variable "env" {
  type    = string
}

variable "owner" {
  type    = string
}

variable "product" {
  type    = string
}

variable "sourceip" {
  type    = string
}

variable "password" {
  type    = string
}