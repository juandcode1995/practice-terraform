# infrastructure-code

Azurern Infrastructure as code initial using Azure

## Steps to deploy:
1. Dowload terraform 1.0.0
2. Clone the repo
3. Open provider file and setup your service principal keys(How to get sevice principal keys: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret)
4. Verify your variables configuration
4. On the root folder initialize the terraform project: 
```
terraform init
terraform plan
terraform apply
```
5. In the server execute the LCMConfig.ps1 inside c:\LCM\LCMConfig.ps1

