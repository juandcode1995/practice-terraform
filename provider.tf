terraform{
    required_providers{
        azurerm = {
            source = "hashicorp/azurerm"
            version = "=2.46.0"
        }
    }

    backend "local" {
        path = "terraform.tfstate"
  }

}

provider "azurerm"{
    features{}
    subscription_id = "8c855d26-2d76-4572-a34d-11c453fadf41"
    client_id       = "b37e1cbc-060d-4834-8068-f3ec92681f9b"
    client_secret   = "nogHBL5o6xP~TBLx_2fg68S4Mcfb9JW4dI" # Here goes the service principal secret
    tenant_id       = "54e59b21-7694-4473-b53b-a78bbd488dda"
}

# You can also set environment variables in yous host so terraform can get keys from there
#export ARM_CLIENT_ID="00000000-0000-0000-0000-000000000000"
#export ARM_CLIENT_SECRET="00000000-0000-0000-0000-000000000000"
#export ARM_SUBSCRIPTION_ID="00000000-0000-0000-0000-000000000000"
#export ARM_TENANT_ID="00000000-0000-0000-0000-000000000000"