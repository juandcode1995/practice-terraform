location = "centralus"

env = "prod"

owner = "DevopsTraining"

product = "codec"

# Allowed ip to RDP to instance
sourceip = "0.0.0.0/0"

#Override password value on deployment, or implement vault keys
password = "P@$$w0rd1234!"